package com.company.main;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class Calculator extends JFrame implements ActionListener{
    private JTextField num1Text;
    private JTextField num2Text;
    private JLabel ansLabel;
    private JButton equalsButton;


    Calculator(){
        setSize(500,300);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setLayout(new FlowLayout());
        num1Text=new JTextField(10);
        num2Text=new JTextField(10);
        add(num1Text);
        add(new JLabel("+"));
        add(num2Text);
        equalsButton=new JButton(" = ");

        // add action listener
        equalsButton.addActionListener(this);
        add(equalsButton);
        ansLabel=new JLabel("");
        add(ansLabel);
        pack();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        String sNum1=num1Text.getText();
        String sNum2=num2Text.getText();
        int x=Integer.parseInt(sNum1);
        int y=Integer.parseInt(sNum2);
        int z=x+y; //
        ansLabel.setText(""+z);
    }
}
